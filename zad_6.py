# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 19:57:37 2016

@author: Matej
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as met
plt.close("all")

def generate_data(n):
    
 #prva klasa
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 = int(n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 indices = np.random.permutation(n)
 data = data[indices,:]
 return data

np.random.seed(242)
podaci = generate_data(200)
np.random.seed(12)
test = generate_data(100)

#plt.figure(1)
#plt.scatter(podaci[:,0],podaci[:,1], c=podaci[:,2], cmap='spring')

xt=np.column_stack((podaci[:,0], podaci[:,1]))
#xt=np.column_stack((np.ones([200,1]), xt1))
yt=podaci[:,2]
logr=lm.LogisticRegression()
logfit=logr.fit(xt, yt)

#print(logr.intercept_, logr.coef_)

go=logr.intercept_ + np.dot(logr.coef_[0,0], podaci[:,0])+np.dot(logr.coef_[0,1], podaci[:,1])
x2=(-logr.intercept_ -np.dot(logr.coef_[0,0], podaci[:,0])) / logr.coef_[0,1]

#plt.plot(podaci[:,0], x2)
#plt.show()
data_train=podaci
LogRegModel=logr

#f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(data_train[:,0])-0.5:max(data_train[:,0])+0.5:.05,
 min(data_train[:,1])-0.5:max(data_train[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = LogRegModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)
#cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="spring", vmin=0, vmax=1)
#ax_c = f.colorbar(cont)
#ax_c.set_label("$P(y = 1|\mathbf{x})$")
#ax_c.set_ticks([0, .25, .5, .75, 1])
#ax.set_xlabel('$x_1$', alpha=0.9)
#ax.set_ylabel('$x_2$', alpha=0.9)
#ax.set_title('Izlaz logističke regresije')
#plt.scatter(podaci[:,0],podaci[:,1], c=podaci[:,2], cmap='spring')
#plt.plot(podaci[:,0], x2)

#plt.show()


#plt.figure(3)
xtest=np.column_stack((test[:,0], test[:,1]))
pred=logr.predict(xtest)
cd=pred-test[:,2]
#plt.scatter(test[:,0], test[:,1], c=cd, cmap='BuGn_r')

cm=met.confusion_matrix(test[:,2], pred) #matrica zabune,u funkciju ulaze stvarne i predvidene klase testnih podataka

def plot_confusion_matrix(c_matrix):

 norm_conf = []
 for i in c_matrix:
    a = 0
    tmp_arr = []
    a = sum(i, 0)
    for j in i:
         tmp_arr.append(float(j)/float(a))
    norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in range(width):
     for y in range(height):
         ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                     horizontalalignment='center',
                     verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])

 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()


plot_confusion_matrix(cm)   #poziv funkcije za prikaz matrice zabune
