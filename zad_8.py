# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 20:05:45 2016

@author: Matej
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn import neighbors
plt.close("all")

def generate_data(n):
    
 #prva klasa
 n1 = int(n/2)
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 = int(n - n/2)
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 indices = np.random.permutation(n)
 data = data[indices,:]
 return data

np.random.seed(242)
podaci = generate_data(200)
np.random.seed(12)
test = generate_data(100)


def plot_KNN(KNN_model, X, y):

 x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
 x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
 xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
 np.arange(x2_min, x2_max, 0.01))

 Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
 Z = Z1.reshape(xx.shape)
 plt.figure()
 plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)
 plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu',
 edgecolor='white', label = 'train')
 
ulazne=podaci[:,[0,1]]  #odvajanje stupaca ulaznih od stupca izlaznih velicina
izlazne=podaci[:,2]

ulaz_s=preprocessing.scale(ulazne)    #skaliranje ulaznih vrijednosti  
#izlazne vrijednosti nema potrebe skalirati, sve su 1 i 0

km=neighbors.KNeighborsRegressor(n_neighbors=2).fit(ulaz_s, izlazne)    #model prima skalirane ulazne podatke i izlazne podatke(klase)
plot_KNN(km, ulaz_s, izlazne)           #poziv funkcije
